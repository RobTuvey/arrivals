﻿namespace Arrivals.Models;

public sealed record PagedResult<TItem>(
    int Start,
    int Count,
    int Total,
    IEnumerable<TItem> items,
    bool HasNextPage
)
{
    public static PagedResult<TItem> Create(int start, int count, IEnumerable<TItem> items)
    {
        int total = items.Count();

        IEnumerable<TItem> pagedItems = items.Skip(start).Take(count);

        return new PagedResult<TItem>(start, count, total, pagedItems, total > (start + count));
    }
}
