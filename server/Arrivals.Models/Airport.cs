﻿namespace Arrivals.Models;

public enum Airport
{
    None = 0,
    Heathrow = 1,
    Stansted = 2
}
