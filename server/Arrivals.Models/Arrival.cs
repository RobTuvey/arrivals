﻿namespace Arrivals.Models;

public sealed record Arrival(
    string Id,
    string FlightNumber,
    string From,
    string FromCode,
    TimeOnly Time,
    string Status
);
