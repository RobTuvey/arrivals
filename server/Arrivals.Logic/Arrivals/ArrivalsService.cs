﻿using Arrivals.Logic.Airports;
using Arrivals.Models;
using Autofac.Features.Indexed;

namespace Arrivals.Logic.Arrivals;

public interface IArrivalsService
{
    /// <summary>
    /// Get a list of arrivals for the specified filters.
    /// </summary>
    /// <param name="startDate">The lower range for the date filter.</param>
    /// <param name="endDate">The upper range for the date filter.</param>
    /// <param name="search">The search term for searching on flight number.</param>
    /// <param name="airportName">The name of the airport.</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<Arrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        string search,
        string airportName,
        CancellationToken cancellationToken
    );

    /// <summary>
    /// Get a list of supported airports.
    /// </summary>
    /// <returns></returns>
    IEnumerable<string> GetAirports();
}

internal class ArrivalsService(
    IIndex<Airport, IAirportService> _keyedAirportServices,
    IEnumerable<IAirportService> _airportServices
) : IArrivalsService
{
    /// <inheritdoc/>
    public async Task<IEnumerable<Arrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        string search,
        string airportName,
        CancellationToken cancellationToken
    )
    {
        if (startDate == DateTime.MinValue)
        {
            throw new ArgumentException("Invalid start date");
        }

        Airport airport = ParseAirport(airportName);

        if (airport == Airport.None)
        {
            throw new ArgumentException("Invalid airport");
        }

        if (_keyedAirportServices.TryGetValue(airport, out IAirportService service) == false)
        {
            throw new ArgumentException("Airport not supported");
        }

        IEnumerable<Arrival> arrivals = await service.GetArrivals(
            startDate,
            endDate == DateTime.MinValue ? startDate.Date.AddDays(1).AddSeconds(-1) : endDate,
            search,
            cancellationToken
        );

        return arrivals.OrderBy(arrival => arrival.Time);
    }

    /// <inheritdoc/>
    public IEnumerable<string> GetAirports()
    {
        return _airportServices.Select(service => service.GetAirport().ToString());
    }

    private Airport ParseAirport(string airport)
    {
        if (Enum.TryParse(airport, out Airport result))
        {
            return result;
        }

        return Airport.None;
    }
}
