﻿using Arrivals.Logic.Airports;
using Arrivals.Logic.Airports.Heathrow;
using Arrivals.Logic.Airports.Stansted;
using Arrivals.Logic.Arrivals;
using Arrivals.Models;
using Autofac;

namespace Arrivals.Logic;

public class ContainerModule : Autofac.Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder
            .RegisterType<HeathrowService>()
            .Keyed<IAirportService>(Airport.Heathrow)
            .As<IAirportService>();

        builder
            .RegisterType<StanstedService>()
            .Keyed<IAirportService>(Airport.Stansted)
            .As<IAirportService>();

        builder.RegisterType<ArrivalsService>().As<IArrivalsService>();

        base.Load(builder);
    }
}
