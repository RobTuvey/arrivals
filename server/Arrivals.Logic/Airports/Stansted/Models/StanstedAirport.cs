﻿namespace Arrivals.Logic.Airports.Stansted.Models;

public sealed record StanstedAirport
{
    public string Name { get; set; } = string.Empty;
    public string Code { get; set; } = string.Empty;
}
