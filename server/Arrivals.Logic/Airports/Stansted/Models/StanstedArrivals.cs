﻿namespace Arrivals.Logic.Airports.Stansted.Models;

public sealed record StanstedArrivals
{
    public IEnumerable<StanstedArrival> AllArrivals { get; set; } =
        Enumerable.Empty<StanstedArrival>();
}
