﻿namespace Arrivals.Logic.Airports.Stansted.Models;

public sealed record StanstedResponse
{
    public required StanstedArrivals Data { get; set; }
}
