﻿namespace Arrivals.Logic.Airports.Stansted.Models;

public sealed record StanstedArrival
{
    public string Id { get; set; } = string.Empty;
    public string FlightNumber { get; set; } = string.Empty;
    public string EstimatedArrivalDateTime { get; set; } = string.Empty;
    public string Status { get; set; } = string.Empty;
    public StanstedAirport? DepartureAirport { get; set; }
}
