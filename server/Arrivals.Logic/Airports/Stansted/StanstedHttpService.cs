﻿using Arrivals.Logic.Airports.Stansted.Models;
using Arrivals.Logic.Exceptions;
using Arrivals.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http.Json;
using System.Text.Json;

namespace Arrivals.Logic.Airports.Stansted;

public interface IStanstedHttpService
{
    /// <summary>
    /// Fetch arrivals from the Stansted API.
    /// </summary>
    /// <param name="startDate">The lower range for the date filter.</param>
    /// <param name="endDate">The upper range for the date filter.</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<StanstedArrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        CancellationToken cancellationToken
    );
}

public class StanstedHttpService(HttpClient _httpClient) : IStanstedHttpService
{
    private const string BASE_URL = "BaseUrl";
    private const string API_KEY = "ApiKey";
    private const string API_KEY_HEADER = "x-api-key";
    private const string GRAPHQL_ROUTE = "graphql";
    private const string QUERY =
        "\nquery($airportCode: String!, $startDate: AWSDateTime, $endDate: AWSDateTime, $size: Int, $from: Int){\n  allArrivals(tenant: $airportCode, startDate: $startDate, endDate: $endDate, size: $size from: $from){\n    id\n    estimatedArrivalDateTime\n    departureAirport {\n      name\n      code\n    }\n    flightNumber\n    status\n  }\n}\n";
    private const string STANSTED_CODE = "STN";
    private const int PAGE_SIZE = 1000;

    private readonly JsonSerializerOptions _jsonOptions = new JsonSerializerOptions
    {
        PropertyNameCaseInsensitive = true
    };

    /// <inheritdoc/>
    public async Task<IEnumerable<StanstedArrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        CancellationToken cancellationToken
    )
    {
        HttpContent content = JsonContent.Create(
            new
            {
                query = QUERY,
                variables = new
                {
                    airportCode = STANSTED_CODE,
                    size = PAGE_SIZE,
                    startDate = startDate.ToString("s") + "Z",
                    endDate = endDate.ToString("s") + "Z",
                }
            }
        );

        HttpResponseMessage response = await _httpClient.PostAsync(
            GRAPHQL_ROUTE,
            content,
            cancellationToken
        );

        StanstedResponse? stanstedResponse =
            await response.Content.ReadFromJsonAsync<StanstedResponse>(
                _jsonOptions,
                cancellationToken
            );

        return stanstedResponse?.Data?.AllArrivals ?? Enumerable.Empty<StanstedArrival>();
    }

    /// <summary>
    /// Configure headers and base address for HttpClient.
    /// </summary>
    /// <param name="serviceProvider">The service provider.</param>
    /// <param name="client">The http client.</param>
    public static void Configure(IServiceProvider serviceProvider, HttpClient client)
    {
        IConfiguration configuration = serviceProvider.GetRequiredService<IConfiguration>();
        IConfigurationSection stanstedConfig = configuration.GetSection(
            Airport.Stansted.ToString()
        );

        ValidateConfiguration(stanstedConfig);

        string? baseUrl = stanstedConfig[BASE_URL];
        string? key = stanstedConfig[API_KEY];

        if (Uri.TryCreate(baseUrl, UriKind.Absolute, out Uri? uri) && uri is not null)
        {
            client.BaseAddress = uri;
            client.DefaultRequestHeaders.Host = uri.Host;
        }

        client.DefaultRequestHeaders.Add(API_KEY_HEADER, key);
    }

    private static void ValidateConfiguration(IConfigurationSection config)
    {
        if (string.IsNullOrEmpty(config[BASE_URL]))
        {
            throw new InvalidConfigurationException("Base url is missing");
        }

        if (string.IsNullOrEmpty(config[API_KEY]))
        {
            throw new InvalidConfigurationException("API key is missing");
        }
    }
}
