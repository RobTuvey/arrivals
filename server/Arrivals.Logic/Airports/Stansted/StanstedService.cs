﻿using Arrivals.Logic.Airports.Stansted.Models;
using Arrivals.Models;

namespace Arrivals.Logic.Airports.Stansted;

internal class StanstedService(IStanstedHttpService _httpService) : IAirportService
{
    /// <inheritdoc/>
    public Airport GetAirport() => Airport.Stansted;

    /// <inheritdoc/>
    public async Task<IEnumerable<Arrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        string search,
        CancellationToken cancellationToken = default
    )
    {
        IEnumerable<StanstedArrival> arrivals = await _httpService.GetArrivals(
            startDate,
            endDate,
            cancellationToken
        );

        if (string.IsNullOrEmpty(search) == false)
        {
            string lowerSearch = search.ToLower();

            IEnumerable<StanstedArrival> searchedArrivals = arrivals.Where(
                arrival => arrival.FlightNumber.ToLower().Contains(lowerSearch)
            );

            return searchedArrivals.Select(MapArrival);
        }

        return arrivals.Select(MapArrival);
    }

    private Arrival MapArrival(StanstedArrival arrival)
    {
        TimeOnly time = TimeOnly.MinValue;
        if (DateTime.TryParse(arrival.EstimatedArrivalDateTime, out DateTime date))
        {
            time = TimeOnly.FromDateTime(date);
        }

        return new Arrival(
            arrival.Id,
            arrival.FlightNumber,
            arrival.DepartureAirport?.Name ?? string.Empty,
            arrival.DepartureAirport?.Code ?? string.Empty,
            time,
            arrival.Status
        );
    }
}
