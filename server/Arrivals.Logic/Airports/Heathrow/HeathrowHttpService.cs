﻿using Arrivals.Logic.Airports.Heathrow.Models;
using Arrivals.Logic.Exceptions;
using Arrivals.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http.Json;
using System.Text.Json;

namespace Arrivals.Logic.Airports.Heathrow;

public interface IHeathrowHttpService
{
    /// <summary>
    /// Fetch arrivals from the Heathrow API.
    /// </summary>
    /// <param name="date">The date to fetch the arrivals for.</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<HeathrowArrival>> GetArrivals(
        DateOnly date,
        CancellationToken cancellationToken = default
    );
}

public class HeathrowHttpService(HttpClient _httpClient) : IHeathrowHttpService
{
    private readonly JsonSerializerOptions _jsonOptions =
        new() { PropertyNameCaseInsensitive = true };

    /// <inheritdoc/>
    public async Task<IEnumerable<HeathrowArrival>> GetArrivals(
        DateOnly date,
        CancellationToken cancellationToken = default
    )
    {
        string dateArgument = date.ToString("yyyy-MM-dd");
        string url = $"pihub/flights/arrivals?date={dateArgument}&orderBy=localArrivalTime";

        HttpResponseMessage response = await _httpClient.GetAsync(url, cancellationToken);

        IEnumerable<HeathrowArrival>? arrivals = await response.Content.ReadFromJsonAsync<
            IEnumerable<HeathrowArrival>
        >(_jsonOptions, cancellationToken);

        return arrivals ?? Enumerable.Empty<HeathrowArrival>();
    }

    /// <summary>
    /// Configure headers and base address for HttpClient.
    /// </summary>
    /// <param name="serviceProvider">The service provider.</param>
    /// <param name="client">The http client.</param>
    public static void Configure(IServiceProvider serviceProvider, HttpClient client)
    {
        IConfiguration config = serviceProvider.GetRequiredService<IConfiguration>();
        IConfigurationSection section = config.GetSection(Airport.Heathrow.ToString());

        ValidateConfiguration(section);

        string? baseUrl = section["BaseUrl"];
        string? origin = section["Origin"];

        if (Uri.TryCreate(baseUrl, UriKind.Absolute, out Uri? uri) && uri is not null)
        {
            client.BaseAddress = uri;
            client.DefaultRequestHeaders.Host = uri.Host;
        }

        client.DefaultRequestHeaders.Add("Origin", origin);
    }

    private static void ValidateConfiguration(IConfigurationSection config)
    {
        if (string.IsNullOrEmpty(config["BaseUrl"]))
        {
            throw new InvalidConfigurationException("Base url configuration is missing");
        }

        if (string.IsNullOrEmpty(config["Origin"]))
        {
            throw new InvalidConfigurationException("Origin configuration is missing");
        }
    }
}
