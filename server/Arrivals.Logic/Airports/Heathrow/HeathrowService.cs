﻿using Arrivals.Logic.Airports.Heathrow.Models;
using Arrivals.Models;

namespace Arrivals.Logic.Airports.Heathrow;

internal class HeathrowService(IHeathrowHttpService _httpService) : IAirportService
{
    private const string ORIGIN = "ORIGIN";
    private const string DESTINATION = "DESTINATION";
    private const string DESTINATION_STATUS = "DestinationStatus";

    /// <inheritdoc/>
    public Airport GetAirport() => Airport.Heathrow;

    /// <inheritdoc/>
    public async Task<IEnumerable<Arrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        string search,
        CancellationToken cancellationToken = default
    )
    {
        DateOnly date = DateOnly.FromDateTime(startDate);

        IEnumerable<HeathrowArrival> arrivals = await _httpService.GetArrivals(
            date,
            cancellationToken
        );

        arrivals = arrivals.Where(
            arrival =>
                arrival.FlightService.AircraftMovement.Route.PortsOfCall.Any(
                    port =>
                        port.PortOfCallType == DESTINATION
                        && port.OperatingTimes?.Scheduled != null
                        && startDate < port.OperatingTimes?.Scheduled?.Utc
                        && endDate > port.OperatingTimes?.Scheduled?.Utc
                )
        );

        if (string.IsNullOrEmpty(search) == false)
        {
            string lowerSearch = search.ToLower();

            IEnumerable<HeathrowArrival> searchedArrivals = arrivals.Where(
                arrival =>
                    arrival.FlightService.IataFlightIdentifier.ToLower().Contains(lowerSearch)
            );

            return searchedArrivals.Select(MapArrival);
        }

        return arrivals.Select(MapArrival);
    }

    internal Arrival MapArrival(HeathrowArrival arrival)
    {
        HeathrowPortOfCall? departureAirport =
            arrival.FlightService.AircraftMovement.Route.PortsOfCall.FirstOrDefault(
                port => port.PortOfCallType == ORIGIN
            );

        HeathrowPortOfCall? destinationAirport =
            arrival.FlightService.AircraftMovement.Route.PortsOfCall.FirstOrDefault(
                port => port.PortOfCallType == DESTINATION
            );

        HeathrowAircraftMovementStatus? destinationStatus =
            arrival.FlightService.AircraftMovement.AircraftMovementStatus.FirstOrDefault(
                status => status.Name == DESTINATION_STATUS
            );

        TimeOnly time = TimeOnly.MinValue;
        if (destinationAirport?.OperatingTimes?.Scheduled?.Utc is not null)
        {
            time = TimeOnly.FromDateTime(destinationAirport.OperatingTimes.Scheduled.Utc);
        }

        return new Arrival(
            arrival.FlightService.IataFlightIdentifier,
            arrival.FlightService.IataFlightIdentifier,
            departureAirport?.AirportFacility.AirportCityLocation.Name ?? string.Empty,
            departureAirport?.AirportFacility.IataIdentifier ?? string.Empty,
            time,
            destinationStatus?.Message ?? string.Empty
        );
    }
}
