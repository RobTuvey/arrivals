﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowRoute
{
    public IEnumerable<HeathrowPortOfCall> PortsOfCall { get; set; } =
        Enumerable.Empty<HeathrowPortOfCall>();
}
