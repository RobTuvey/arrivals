﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowAircraftMovement
{
    public required HeathrowRoute Route { get; set; }
    public IEnumerable<HeathrowAircraftMovementStatus> AircraftMovementStatus { get; set; } =
        Enumerable.Empty<HeathrowAircraftMovementStatus>();
}
