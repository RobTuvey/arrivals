﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowAirportCityLocation
{
    public string Name { get; set; } = string.Empty;
}
