﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowScheduledOperatingTimes
{
    public DateTime Utc { get; set; }
}
