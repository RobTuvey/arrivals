﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowArrival
{
    public required HeathrowFlightService FlightService { get; set; }
};
