﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowPortOfCall
{
    public string PortOfCallType { get; set; } = string.Empty;
    public required HeathrowAirportFacility AirportFacility { get; set; }
    public HeathrowOperatingTimes? OperatingTimes { get; set; }
}
