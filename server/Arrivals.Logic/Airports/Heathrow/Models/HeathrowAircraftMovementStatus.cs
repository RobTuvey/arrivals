﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowAircraftMovementStatus
{
    public string Name { get; set; } = string.Empty;
    public string Message { get; set; } = string.Empty;
}
