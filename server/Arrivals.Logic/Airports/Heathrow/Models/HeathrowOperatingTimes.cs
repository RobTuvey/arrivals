﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowOperatingTimes
{
    public HeathrowScheduledOperatingTimes? Scheduled { get; set; }
}
