﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowFlightService
{
    public string IataFlightIdentifier { get; set; } = string.Empty;

    public required HeathrowAircraftMovement AircraftMovement { get; set; }
}
