﻿namespace Arrivals.Logic.Airports.Heathrow.Models;

public sealed record HeathrowAirportFacility
{
    public string IataIdentifier { get; set; } = string.Empty;
    public required HeathrowAirportCityLocation AirportCityLocation { get; set; }
}
