﻿using Arrivals.Models;

namespace Arrivals.Logic.Airports;

internal interface IAirportService
{
    /// <summary>
    /// Get the type of airport.
    /// </summary>
    /// <returns></returns>
    Airport GetAirport();

    /// <summary>
    /// Get a list of arrivals using the specified filters.
    /// </summary>
    /// <param name="startDate">Get arrivals arriving on this date.</param>
    /// <param name="endDate">Get arrivals up to this end date.</param>
    /// <param name="search">The search term.</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<Arrival>> GetArrivals(
        DateTime startDate,
        DateTime endDate,
        string search,
        CancellationToken cancellationToken = default
    );
}
