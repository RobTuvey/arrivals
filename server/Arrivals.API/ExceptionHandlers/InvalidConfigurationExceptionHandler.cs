﻿using Arrivals.Logic.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Arrivals.API.ExceptionHandlers;

public class InvalidConfigurationExceptionHandler : IExceptionHandler
{
    public async ValueTask<bool> TryHandleAsync(
        HttpContext httpContext,
        Exception exception,
        CancellationToken cancellationToken
    )
    {
        if (exception is InvalidConfigurationException)
        {
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await httpContext.Response.WriteAsJsonAsync(
                new ProblemDetails
                {
                    Title = "Invalid configuration",
                    Detail = exception.Message,
                    Type = nameof(InvalidConfigurationException),
                    Status = (int)HttpStatusCode.InternalServerError
                },
                cancellationToken
            );

            return true;
        }

        return false;
    }
}
