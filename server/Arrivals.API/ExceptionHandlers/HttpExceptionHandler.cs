﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Arrivals.API.ExceptionHandlers;

public class HttpExceptionHandler : IExceptionHandler
{
    public async ValueTask<bool> TryHandleAsync(
        HttpContext httpContext,
        Exception exception,
        CancellationToken cancellationToken
    )
    {
        if (exception is HttpRequestException)
        {
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            await httpContext.Response.WriteAsJsonAsync(
                new ProblemDetails
                {
                    Title = "Something went wrong talking to a third party service",
                    Detail = exception.Message,
                    Type = nameof(HttpRequestException),
                    Status = (int)HttpStatusCode.InternalServerError
                },
                cancellationToken
            );

            return true;
        }

        return false;
    }
}
