﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Arrivals.API.ExceptionHandlers;

public class ArgumentExceptionHandler : IExceptionHandler
{
    public async ValueTask<bool> TryHandleAsync(
        HttpContext httpContext,
        Exception exception,
        CancellationToken cancellationToken
    )
    {
        if (exception is ArgumentException)
        {
            httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            await httpContext.Response.WriteAsJsonAsync(
                new ProblemDetails
                {
                    Title = "Invalid parameters",
                    Detail = exception.Message,
                    Type = nameof(ArgumentException),
                    Status = (int)HttpStatusCode.BadRequest,
                },
                cancellationToken
            );

            return true;
        }

        return false;
    }
}
