﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Arrivals.API.ExceptionHandlers;

public class KeyNotFoundExceptionHandler : IExceptionHandler
{
    public async ValueTask<bool> TryHandleAsync(
        HttpContext httpContext,
        Exception exception,
        CancellationToken cancellationToken
    )
    {
        if (exception is KeyNotFoundException)
        {
            httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
            await httpContext.Response.WriteAsJsonAsync(
                new ProblemDetails
                {
                    Title = "Not found",
                    Detail = exception.Message,
                    Type = nameof(KeyNotFoundException),
                    Status = (int)HttpStatusCode.NotFound
                },
                cancellationToken
            );

            return true;
        }

        return false;
    }
}
