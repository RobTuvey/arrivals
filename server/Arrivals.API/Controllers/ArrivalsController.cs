﻿using Arrivals.Logic.Arrivals;
using Arrivals.Models;
using Microsoft.AspNetCore.Mvc;

namespace Arrivals.API.Controllers;

/// <summary>
/// API controller for fetching arrivals and related information.
/// </summary>
/// <param name="_arrivalsService"></param>
[ApiController]
[Route("[controller]")]
public class ArrivalsController(IArrivalsService _arrivalsService) : ControllerBase
{
    /// <summary>
    /// Get a list of arrivals for a given airport.
    /// </summary>
    /// <param name="airport">The airport to get arrivals for.</param>
    /// <param name="startDate">The lower range of the date filter.</param>
    /// <param name="endDate">The upper range of the date filter.</param>
    /// <param name="start">The page index.</param>
    /// <param name="count">The page length.</param>
    /// <param name="search">The search term for searching the flight number.</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<IActionResult> GetArrivals(
        [FromQuery] string airport,
        [FromQuery] DateTime startDate,
        [FromQuery] DateTime endDate,
        [FromQuery] int start,
        [FromQuery] int count,
        [FromQuery] string? search,
        CancellationToken cancellationToken
    )
    {
        IEnumerable<Arrival> arrivals = await _arrivalsService.GetArrivals(
            startDate,
            endDate,
            search ?? string.Empty,
            airport,
            cancellationToken
        );

        return Ok(PagedResult<Arrival>.Create(start, count, arrivals));
    }

    /// <summary>
    /// Get a list of supported airports.
    /// </summary>
    /// <returns></returns>
    [HttpGet("airports")]
    public IActionResult GetAirports()
    {
        IEnumerable<string> airports = _arrivalsService.GetAirports();

        return Ok(airports);
    }
}
