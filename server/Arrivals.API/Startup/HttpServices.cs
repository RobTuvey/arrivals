﻿using Arrivals.Logic.Airports.Heathrow;
using Arrivals.Logic.Airports.Stansted;
using System.Net;

namespace Arrivals.API.Startup;

public static class HttpServices
{
    /// <summary>
    /// Configure http services for making requests to third party systems.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <returns></returns>
    public static IServiceCollection ConfigureHttpServices(this IServiceCollection services)
    {
        services
            .AddHttpClient<IHeathrowHttpService, HeathrowHttpService>(HeathrowHttpService.Configure)
            .ConfigurePrimaryHttpMessageHandler(
                _ =>
                    new HttpClientHandler
                    {
                        AutomaticDecompression =
                            DecompressionMethods.Deflate | DecompressionMethods.GZip
                    }
            );

        services.AddHttpClient<IStanstedHttpService, StanstedHttpService>(
            StanstedHttpService.Configure
        );

        return services;
    }
}
