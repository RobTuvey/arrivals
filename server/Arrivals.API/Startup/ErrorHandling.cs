﻿using Arrivals.API.ExceptionHandlers;

namespace Arrivals.API.Startup;

public static class ErrorHandling
{
    /// <summary>
    /// Confiugre error handlers for serving problem details.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <returns></returns>
    public static IServiceCollection ConfigureErrorHandlers(this IServiceCollection services)
    {
        services.AddExceptionHandler<ArgumentExceptionHandler>();
        services.AddExceptionHandler<KeyNotFoundExceptionHandler>();
        services.AddExceptionHandler<InvalidConfigurationExceptionHandler>();
        services.AddExceptionHandler<HttpExceptionHandler>();

        return services;
    }
}
