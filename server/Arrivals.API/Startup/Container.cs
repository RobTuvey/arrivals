﻿using Arrivals.Logic;
using Autofac;

namespace Arrivals.API.Startup;

public static class Container
{
    /// <summary>
    /// Configure the dependency container.
    /// </summary>
    /// <param name="builder">The container builder.</param>
    /// <returns></returns>
    public static ContainerBuilder ConfigureContainer(this ContainerBuilder builder)
    {
        builder.RegisterModule(new ContainerModule());

        return builder;
    }
}
