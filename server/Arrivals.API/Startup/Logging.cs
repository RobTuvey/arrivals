﻿using Serilog;

namespace Arrivals.API.Startup;

public static class Logging
{
    /// <summary>
    /// Configure logging for the application.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <param name="configuration">The system configuration.</param>
    /// <returns></returns>
    public static IServiceCollection ConfigureLogging(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        services.AddLogging(builder =>
        {
            Serilog.ILogger logger = new LoggerConfiguration().Enrich
                .FromLogContext()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            builder.AddSerilog(logger);
        });

        return services;
    }
}
