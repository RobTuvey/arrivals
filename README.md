# Problem

The task is to create a live flight arrival information application using server and frontend technologies.

The back end should be pulling live arrival information from Heathrow and Stansted airports. This should then be used to service a web application to display the information.

## Requirements

The backend is written using .Net 8 and C# 12. So the minimum version of Visual Studio that will be supported is 17.8.

# Backend

To run the backend service, open the solution at <code>/server/Arrivals.sln</code> and run the <code>https</code> profile.
Alternatively the backend can be run from the command line from <code>/server/Arrivals.API/</code> by running <code>dotnet run -lp https</code>

# Client

The client can be started simply by navigating into the <code>/client</code> directory and running <code>yarn dev</code>. An instance of the client will then be running at <code>http://localhost:5173</code> or the nearest available port.
