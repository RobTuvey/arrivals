import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./theme";
import { Arrivals } from "./Arrivals";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Arrivals />
    </ThemeProvider>
  );
}

export default App;
