import { createTheme } from "@mui/material";

export const theme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#FFD700",
      light: "#fef167",
      dark: "#ffa700",
      contrastText: "#141414",
    },
    text: {
      primary: "#ffd700",
    },
  },
  typography: {
    fontFamily: "Arial, sans-serif",
  },
});
