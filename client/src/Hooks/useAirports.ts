import { useEffect, useState } from "react";
import { getAirports } from "../Api";

export function useAirports() {
  const [loading, setLoading] = useState(false);
  const [airports, setAirports] = useState<string[]>([]);

  useEffect(() => {
    setLoading(true);
    getAirports()
      .then(setAirports)
      .finally(() => setLoading(false));
  }, []);

  return {
    airports,
    loading,
  };
}
