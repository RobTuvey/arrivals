import { useEffect, useState } from "react";
import { getArrivals } from "../Api";

const PAGE_SIZE = 15;

export function useArrivals(airport: Maybe<string>, search?: Maybe<string>) {
  const [index, setIndex] = useState(0);
  const [hasNextPage, setHasNextPage] = useState(false);
  const [arrivals, setArrivals] = useState<Arrival[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setIndex(0);
    setArrivals([]);
  }, [airport, search]);

  useEffect(() => {
    if (airport) {
      setLoading(true);
      getArrivals({
        airport,
        start: index * PAGE_SIZE,
        count: PAGE_SIZE,
        search,
      })
        .then((result) => {
          setArrivals((current) => [...current, ...result.items]);
          setHasNextPage(result.hasNextPage);
        })
        .finally(() => setLoading(false));
    }
  }, [index, airport, search]);

  return {
    next: () => hasNextPage && setIndex((current) => current + 1),
    hasNextPage: !loading && hasNextPage,
    arrivals,
    loading,
  };
}
