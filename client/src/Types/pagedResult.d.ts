declare type PagedResult<TItem> = {
  start: number;
  count: number;
  total: number;
  items: TItem[];
  hasNextPage: boolean;
};
