declare type Arrival = {
  id: string;
  flightNumber: string;
  from: string;
  fromCode: string;
  time: string;
  status: string;
};
