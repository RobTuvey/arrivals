import { Typography } from "@mui/material";

export interface ISpacedTextProps {
  children: string;
  pad: number;
}

export function SpacedText({ children, pad }: ISpacedTextProps) {
  return (
    <Typography variant="h6" color="textPrimary">
      {children
        .trim()
        .replace(" ", "\u00A0")
        .padEnd(pad, "\u00A0")
        .slice(0, pad)
        .split("")
        .map((char, index) => (
          <span
            key={index}
            style={{
              backgroundColor: "#262626",
              borderRadius: "4px",
              padding: "8px 6px",
              margin: "0px 4px",
              fontFamily: "monospace, monospace",
            }}
          >
            {char}
          </span>
        ))}
    </Typography>
  );
}
