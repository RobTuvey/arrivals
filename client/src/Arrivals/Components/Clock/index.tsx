import { Typography } from "@mui/material";
import { format } from "date-fns";
import { useEffect, useState } from "react";

export function Clock() {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(new Date());
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <Typography variant="h2" color="textPrimary">
      {format(time, "HH:mm")}
    </Typography>
  );
}
