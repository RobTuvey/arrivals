import { Typography, Stack } from "@mui/material";

import { ArrivalListItem } from "..";

import "./index.css";

export interface IArrivalsTableProps {
  arrivals: Arrival[];
}

export function ArrivalsTable({ arrivals }: IArrivalsTableProps) {
  return (
    <Stack sx={{ minWidth: "75vw" }}>
      <table>
        <thead>
          <tr>
            <th style={{ width: "10%" }}>
              <Typography
                variant="h4"
                textTransform="uppercase"
                color="textPrimary"
              >
                Time
              </Typography>
            </th>
            <th style={{ width: "40%" }}>
              <Typography
                variant="h4"
                textTransform="uppercase"
                color="textPrimary"
              >
                From
              </Typography>
            </th>
            <th style={{ width: "15%" }}>
              <Typography
                variant="h4"
                textTransform="uppercase"
                color="textPrimary"
              >
                Flight No.
              </Typography>
            </th>
            <th style={{ width: "25%" }}>
              <Typography
                variant="h4"
                textTransform="uppercase"
                color="textPrimary"
              >
                Status
              </Typography>
            </th>
          </tr>
        </thead>
        <tbody>
          {arrivals.map((arrival) => (
            <ArrivalListItem key={arrival.id} arrival={arrival} />
          ))}
        </tbody>
      </table>
    </Stack>
  );
}
