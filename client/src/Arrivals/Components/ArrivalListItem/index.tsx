import { SpacedText } from "..";

export interface IArrivalListItemProps {
  arrival: Arrival;
}

export function ArrivalListItem({ arrival }: IArrivalListItemProps) {
  return (
    <tr>
      <td>
        <SpacedText pad={6}>{arrival.time.slice(0, 5)}</SpacedText>
      </td>
      <td>
        <SpacedText pad={25}>
          {`${arrival.from}${arrival.fromCode ? `(${arrival.fromCode})` : ""}`}
        </SpacedText>
      </td>
      <td>
        <SpacedText pad={8}>{arrival.flightNumber}</SpacedText>
      </td>
      <td>
        <SpacedText pad={20}>{arrival.status}</SpacedText>
      </td>
    </tr>
  );
}
