import {
  Box,
  Typography,
  Stack,
  Select,
  MenuItem,
  Button,
  TextField,
  CircularProgress,
} from "@mui/material";
import { FlightLand } from "@mui/icons-material";
import { useTheme } from "@mui/material/styles";
import { ArrivalsTable, Clock } from "./Components";
import { useAirports, useArrivals, useDebounce } from "../Hooks";
import { useEffect, useState } from "react";

export function Arrivals() {
  const { palette } = useTheme();
  const [airport, setAirport] = useState("");
  const [search, setSearch] = useState("");
  const debouncedSearch = useDebounce(search, 400);

  const { arrivals, next, hasNextPage, loading } = useArrivals(
    airport,
    debouncedSearch
  );
  const { airports } = useAirports();

  useEffect(() => {
    if (airports.length > 1 && !airport) {
      setAirport(airports[0]);
    }
  }, [airports]);

  return (
    <Stack
      sx={{ paddingTop: "64px", paddingBottom: "32px", width: "100%", flex: 1 }}
      spacing={4}
      alignItems="center"
    >
      <Stack direction="column" sx={{ width: "75vw" }}>
        <Stack direction="row" spacing={3} justifyContent="center">
          <Box
            sx={{
              padding: "8px",
              backgroundColor: palette.primary.main,
              borderRadius: "8px",
            }}
          >
            <FlightLand sx={{ fontSize: "6rem" }} />
          </Box>
          <Typography
            variant="h1"
            color="textPrimary"
            textTransform="uppercase"
          >
            Arrivals
          </Typography>
        </Stack>
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Stack direction="row" alignItems="center" spacing={2}>
            <Select
              value={airport}
              onChange={(event) => setAirport(event.target.value as string)}
            >
              {airports.map((airport) => (
                <MenuItem key={airport} value={airport}>
                  {airport}
                </MenuItem>
              ))}
            </Select>
            <TextField
              label="Search"
              placeholder="Search"
              sx={{ color: palette.primary.main }}
              onChange={(event) => setSearch(event.target.value as string)}
            />
          </Stack>
          <Clock />
        </Stack>
      </Stack>
      <ArrivalsTable arrivals={arrivals} />
      {loading && (
        <Stack flex={1} justifyContent="center" alignItems="center">
          <CircularProgress size="6rem" />
        </Stack>
      )}
      <Stack direction="row" justifyContent="center">
        {hasNextPage && <Button onClick={next}>Load More</Button>}
      </Stack>
    </Stack>
  );
}
