import { api } from ".";

export async function getAirports() {
  try {
    const response = await api.get<string[]>("arrivals/airports");

    return response.data;
  } catch {
    return [];
  }
}
