import { format, addMinutes } from "date-fns";
import { api } from ".";

export interface IGetArrivalsProps {
  airport: string;
  start: number;
  count: number;
  search?: Maybe<string>;
}

export async function getArrivals({
  airport,
  start,
  count,
  search,
}: IGetArrivalsProps) {
  const params = new URLSearchParams();

  params.append("airport", airport);
  params.append("start", start.toString());
  params.append("count", count.toString());
  params.append(
    "startDate",
    format(addMinutes(new Date(), -30), "yyyy-MM-dd HH:mm")
  );
  search && params.append("search", search);

  try {
    const response = await api.get<PagedResult<Arrival>>(
      `arrivals?${params.toString()}`
    );

    return response.data;
  } catch {
    return {
      start: 0,
      count: 0,
      total: 0,
      items: [],
      hasNextPage: false,
    };
  }
}
