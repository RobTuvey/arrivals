import axios from "axios";

export * from "./getArrivals";
export * from "./getAirports";

export const api = axios.create();

api.defaults.baseURL = import.meta.env.VITE_BASE_URL;
api.defaults.headers.common.Accept = "application/json";
