export const arrivals: Arrival[] = [
  {
    id: "1",
    flightNumber: "ABC123",
    from: "New York",
    fromCode: "JFK",
    time: "08:00",
    status: "On Time",
  },
  {
    id: "2",
    flightNumber: "XYZ789",
    from: "London",
    fromCode: "LHR",
    time: "09:30",
    status: "Delayed",
  },
  {
    id: "3",
    flightNumber: "DEF456",
    from: "Tokyo",
    fromCode: "NRT",
    time: "11:15",
    status: "Boarding",
  },
  {
    id: "4",
    flightNumber: "GHI789",
    from: "Paris",
    fromCode: "CDG",
    time: "13:45",
    status: "Arrived",
  },
  {
    id: "5",
    flightNumber: "JKL012",
    from: "Sydney",
    fromCode: "SYD",
    time: "15:20",
    status: "Scheduled",
  },
  {
    id: "6",
    flightNumber: "MNO345",
    from: "Dubai",
    fromCode: "DXB",
    time: "17:00",
    status: "On Time",
  },
  {
    id: "7",
    flightNumber: "PQR678",
    from: "Beijing",
    fromCode: "PEK",
    time: "18:30",
    status: "Delayed",
  },
  {
    id: "8",
    flightNumber: "STU901",
    from: "Los Angeles",
    fromCode: "LAX",
    time: "20:15",
    status: "Boarding",
  },
  {
    id: "9",
    flightNumber: "VWX234",
    from: "Singapore",
    fromCode: "SIN",
    time: "22:00",
    status: "Arrived",
  },
  {
    id: "10",
    flightNumber: "YZA567",
    from: "Toronto",
    fromCode: "YYZ",
    time: "23:45",
    status: "Scheduled",
  },
];
